#[tokio::test]
async fn health_check() {
    spawn_app();

    let client = reqwest::Client::new();

    let response = client
        .get("http://127.0.0.1:3000/healthcheck")
        .send()
        .await
        .expect("Failed to request.");

    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}

fn spawn_app() {
    let service = lasergui::service();

    let _ = tokio::spawn(service.unwrap());
}


use lasergui::service;

#[tokio::main]
async fn main() {
    service().unwrap().await;
}
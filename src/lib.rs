use axum::{
    extract::{
        ws::{Message, WebSocket, WebSocketUpgrade},
        Path, Query,
    },
    http::StatusCode,
    response::{IntoResponse, Json, Response},
    routing::{get, post, IntoMakeService},
    Router, Server,
};
use serde_json::{json, Value};
use std::collections::HashMap;
use time::OffsetDateTime;
use tokio::time::sleep;

const BIND_ADDRESS: &str = "0.0.0.0:3000";

type Service = Server<hyper::server::conn::AddrIncoming, IntoMakeService<Router>>;

pub fn service() -> Result<Service, std::io::Error> {
    tracing_subscriber::fmt::init();
    let app = Router::new()
        .route("/healthcheck", get(health_check))
        .route("/test/path/:id", get(test_path))
        .route("/test/query", get(test_query))
        .route("/test/json", post(test_json))
        .route("/events", get(http_upgrade));

    tracing::info!("listening on {}", &BIND_ADDRESS);

    let server = Server::bind(&BIND_ADDRESS.parse().unwrap()).serve(app.into_make_service());

    Ok(server)
}
async fn health_check() -> impl IntoResponse {
    StatusCode::OK
}

async fn test_path(Path(id): Path<u32>) -> Json<Value> {
    Json(json!({ "id": id }))
}

async fn test_query(
    Query(params): Query<HashMap<String, String>>,
) -> Result<Json<Value>, StatusCode> {
    Ok(Json(serde_json::to_value(params).unwrap()))
}

async fn test_json(payload: Json<Value>) -> Json<Value> {
    payload
}

async fn http_upgrade(ws: WebSocketUpgrade) -> Response {
    ws.on_upgrade(websocket)
}

async fn websocket(mut socket: WebSocket) {
    let mut counter = 0;
    loop {
        let msg = json!({"counter": counter, "datetime": OffsetDateTime::now_utc().to_string()});
        if socket
            .send(Message::Text(serde_json::to_string(&msg).unwrap()))
            .await
            .is_err()
        {
            return;
        }
        counter += 1;
        sleep(std::time::Duration::from_secs(1)).await;
    }
}
